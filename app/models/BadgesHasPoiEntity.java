package models;

import javax.persistence.*;

/**
 * Created by tony on 12/6/14.
 */
@Entity
@Table(name = "Badges_has_POI", schema = "", catalog = "GrifSpot")
@IdClass(BadgesHasPoiEntityPK.class)
public class BadgesHasPoiEntity {
    private int badgesBadgesId;
    private int poiPointId;

    @Id
    @Column(name = "Badges_BadgesID")
    public int getBadgesBadgesId() {
        return badgesBadgesId;
    }

    public void setBadgesBadgesId(int badgesBadgesId) {
        this.badgesBadgesId = badgesBadgesId;
    }

    @Id
    @Column(name = "POI_PointID")
    public int getPoiPointId() {
        return poiPointId;
    }

    public void setPoiPointId(int poiPointId) {
        this.poiPointId = poiPointId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BadgesHasPoiEntity that = (BadgesHasPoiEntity) o;

        if (badgesBadgesId != that.badgesBadgesId) return false;
        if (poiPointId != that.poiPointId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = badgesBadgesId;
        result = 31 * result + poiPointId;
        return result;
    }
}
