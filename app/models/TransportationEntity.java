package models;

import javax.persistence.*;

/**
 * Created by tony on 12/6/14.
 */
@Entity
@Table(name = "Transportation", schema = "", catalog = "GrifSpot")
@IdClass(TransportationEntityPK.class)
public class TransportationEntity {
    private int ptid;
    private int poiPointId;
    private String ptType;
    private String fares;

    @Id
    @Column(name = "PTID")
    public int getPtid() {
        return ptid;
    }

    public void setPtid(int ptid) {
        this.ptid = ptid;
    }

    @Id
    @Column(name = "POI_PointID")
    public int getPoiPointId() {
        return poiPointId;
    }

    public void setPoiPointId(int poiPointId) {
        this.poiPointId = poiPointId;
    }

    @Basic
    @Column(name = "PT_Type")
    public String getPtType() {
        return ptType;
    }

    public void setPtType(String ptType) {
        this.ptType = ptType;
    }

    @Basic
    @Column(name = "Fares")
    public String getFares() {
        return fares;
    }

    public void setFares(String fares) {
        this.fares = fares;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TransportationEntity that = (TransportationEntity) o;

        if (poiPointId != that.poiPointId) return false;
        if (ptid != that.ptid) return false;
        if (fares != null ? !fares.equals(that.fares) : that.fares != null) return false;
        if (ptType != null ? !ptType.equals(that.ptType) : that.ptType != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = ptid;
        result = 31 * result + poiPointId;
        result = 31 * result + (ptType != null ? ptType.hashCode() : 0);
        result = 31 * result + (fares != null ? fares.hashCode() : 0);
        return result;
    }
}
