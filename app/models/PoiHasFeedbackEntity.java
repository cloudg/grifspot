package models;

import javax.persistence.*;

/**
 * Created by tony on 12/6/14.
 */
@Entity
@Table(name = "POI_has_Feedback", schema = "", catalog = "GrifSpot")
@IdClass(PoiHasFeedbackEntityPK.class)
public class PoiHasFeedbackEntity {
    private int poiPointId;
    private int feedbackFeedbackId;

    @Id
    @Column(name = "POI_PointID")
    public int getPoiPointId() {
        return poiPointId;
    }

    public void setPoiPointId(int poiPointId) {
        this.poiPointId = poiPointId;
    }

    @Id
    @Column(name = "Feedback_FeedbackID")
    public int getFeedbackFeedbackId() {
        return feedbackFeedbackId;
    }

    public void setFeedbackFeedbackId(int feedbackFeedbackId) {
        this.feedbackFeedbackId = feedbackFeedbackId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PoiHasFeedbackEntity that = (PoiHasFeedbackEntity) o;

        if (feedbackFeedbackId != that.feedbackFeedbackId) return false;
        if (poiPointId != that.poiPointId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = poiPointId;
        result = 31 * result + feedbackFeedbackId;
        return result;
    }
}
