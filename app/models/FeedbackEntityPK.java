package models;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by tony on 12/6/14.
 */
public class FeedbackEntityPK implements Serializable {
    private int feedbackId;
    private int questionNum;

    @Column(name = "FeedbackID")
    @Id
    public int getFeedbackId() {
        return feedbackId;
    }

    public void setFeedbackId(int feedbackId) {
        this.feedbackId = feedbackId;
    }

    @Column(name = "QuestionNum")
    @Id
    public int getQuestionNum() {
        return questionNum;
    }

    public void setQuestionNum(int questionNum) {
        this.questionNum = questionNum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FeedbackEntityPK that = (FeedbackEntityPK) o;

        if (feedbackId != that.feedbackId) return false;
        if (questionNum != that.questionNum) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = feedbackId;
        result = 31 * result + questionNum;
        return result;
    }
}
