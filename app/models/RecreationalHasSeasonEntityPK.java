package models;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by tony on 12/6/14.
 */
public class RecreationalHasSeasonEntityPK implements Serializable {
    private int recreationalRecreationalId;
    private int seasonSeasonId;

    @Column(name = "Recreational_RecreationalID")
    @Id
    public int getRecreationalRecreationalId() {
        return recreationalRecreationalId;
    }

    public void setRecreationalRecreationalId(int recreationalRecreationalId) {
        this.recreationalRecreationalId = recreationalRecreationalId;
    }

    @Column(name = "Season_SeasonID")
    @Id
    public int getSeasonSeasonId() {
        return seasonSeasonId;
    }

    public void setSeasonSeasonId(int seasonSeasonId) {
        this.seasonSeasonId = seasonSeasonId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RecreationalHasSeasonEntityPK that = (RecreationalHasSeasonEntityPK) o;

        if (recreationalRecreationalId != that.recreationalRecreationalId) return false;
        if (seasonSeasonId != that.seasonSeasonId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = recreationalRecreationalId;
        result = 31 * result + seasonSeasonId;
        return result;
    }
}
