package models;

import play.db.jpa.JPA;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

/**
 * Created by tony on 12/6/14.
 */
@Entity
@Table(name = "POI", schema = "", catalog = "GrifSpot")
public class PoiEntity {
    private int pointId;
    private String name;
    private String description;
    private String address;
    private String phoneNumber;
    private String url;
    private String image;
    private Integer categoryId;
    private Collection<ArchitectureEntity> architecturesByPointId;
    private Collection<BadgesHasPoiEntity> badgesHasPoisByPointId;
    private Collection<CommentsEntity> commentsesByPointId;
    private Collection<EducationEntity> educationsByPointId;
    private Collection<HoursEntity> hoursesByPointId;
    private Collection<PoiHasFeedbackEntity> poiHasFeedbacksByPointId;
    private Collection<RecreationalEntity> recreationalsByPointId;
    private Collection<RestaurantEntity> restaurantsByPointId;
    private Collection<RetailEntity> retailsByPointId;
    private Collection<SocialEntity> socialsByPointId;
    private Collection<TransportationEntity> transportationsByPointId;

    public void update() {
        JPA.em().merge(this);
    }

    public static List<PoiEntity> findAll(){
        return JPA.em()
                .createQuery("Select p from PoiEntity p", PoiEntity.class)
                .getResultList();
    }

    public static List<PoiEntity> findInCategories(List<Integer> categories){
        List<PoiEntity> pois = JPA.em().createQuery("Select p from PoiEntity p where categoryId in (:categories)", PoiEntity.class)
                .setParameter("categories", categories)
                .getResultList();
        return pois;
    }

    public static PoiEntity findById(Integer id){
        return JPA.em().find(PoiEntity.class, id);
    }

    public void save() {
        JPA.em().persist(this);
    }

    public void delete() {JPA.em().remove(this);}

    @Id
    @Column(name = "PointID")
    public int getPointId() {
        return pointId;
    }

    public void setPointId(int pointId) {
        this.pointId = pointId;
    }

    @Basic
    @Column(name = "Name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "Description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "Address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "PhoneNumber")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Basic
    @Column(name = "URL")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Basic
    @Column(name = "Image")
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Basic
    @Column(name = "CategoryID")
    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PoiEntity poiEntity = (PoiEntity) o;

        if (pointId != poiEntity.pointId) return false;
        if (address != null ? !address.equals(poiEntity.address) : poiEntity.address != null) return false;
        if (categoryId != null ? !categoryId.equals(poiEntity.categoryId) : poiEntity.categoryId != null) return false;
        if (description != null ? !description.equals(poiEntity.description) : poiEntity.description != null)
            return false;
        if (image != null ? !image.equals(poiEntity.image) : poiEntity.image != null) return false;
        if (name != null ? !name.equals(poiEntity.name) : poiEntity.name != null) return false;
        if (phoneNumber != null ? !phoneNumber.equals(poiEntity.phoneNumber) : poiEntity.phoneNumber != null)
            return false;
        if (url != null ? !url.equals(poiEntity.url) : poiEntity.url != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = pointId;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (phoneNumber != null ? phoneNumber.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (image != null ? image.hashCode() : 0);
        result = 31 * result + (categoryId != null ? categoryId.hashCode() : 0);
        return result;
    }

    @OneToMany()
    @JoinColumn(name="POI_PointID")
    public Collection<ArchitectureEntity> getArchitecturesByPointId() {
        return architecturesByPointId;
    }

    public void setArchitecturesByPointId(Collection<ArchitectureEntity> architecturesByPointId) {
        this.architecturesByPointId = architecturesByPointId;
    }

    @OneToMany()
    @JoinColumn(name="POI_PointID")
    public Collection<BadgesHasPoiEntity> getBadgesHasPoisByPointId() {
        return badgesHasPoisByPointId;
    }

    public void setBadgesHasPoisByPointId(Collection<BadgesHasPoiEntity> badgesHasPoisByPointId) {
        this.badgesHasPoisByPointId = badgesHasPoisByPointId;
    }

    @OneToMany()
    @JoinColumn(name="POI_PointID")
    public Collection<CommentsEntity> getCommentsesByPointId() {
        return commentsesByPointId;
    }

    public void setCommentsesByPointId(Collection<CommentsEntity> commentsesByPointId) {
        this.commentsesByPointId = commentsesByPointId;
    }

    @OneToMany()
    @JoinColumn(name="POI_PointID")
    public Collection<EducationEntity> getEducationsByPointId() {
        return educationsByPointId;
    }

    public void setEducationsByPointId(Collection<EducationEntity> educationsByPointId) {
        this.educationsByPointId = educationsByPointId;
    }

    @OneToMany()
    @JoinColumn(name="POI_PointID")
    public Collection<HoursEntity> getHoursesByPointId() {
        return hoursesByPointId;
    }

    public void setHoursesByPointId(Collection<HoursEntity> hoursesByPointId) {
        this.hoursesByPointId = hoursesByPointId;
    }

    @OneToMany()
    @JoinColumn(name="POI_PointID")
    public Collection<PoiHasFeedbackEntity> getPoiHasFeedbacksByPointId() {
        return poiHasFeedbacksByPointId;
    }

    public void setPoiHasFeedbacksByPointId(Collection<PoiHasFeedbackEntity> poiHasFeedbacksByPointId) {
        this.poiHasFeedbacksByPointId = poiHasFeedbacksByPointId;
    }

    @OneToMany()
    @JoinColumn(name="POI_PointID")
    public Collection<RecreationalEntity> getRecreationalsByPointId() {
        return recreationalsByPointId;
    }

    public void setRecreationalsByPointId(Collection<RecreationalEntity> recreationalsByPointId) {
        this.recreationalsByPointId = recreationalsByPointId;
    }

    @OneToMany()
    @JoinColumn(name="POI_PointID")
    public Collection<RestaurantEntity> getRestaurantsByPointId() {
        return restaurantsByPointId;
    }

    public void setRestaurantsByPointId(Collection<RestaurantEntity> restaurantsByPointId) {
        this.restaurantsByPointId = restaurantsByPointId;
    }

    @OneToMany()
    @JoinColumn(name="POI_PointID")
    public Collection<RetailEntity> getRetailsByPointId() {
        return retailsByPointId;
    }

    public void setRetailsByPointId(Collection<RetailEntity> retailsByPointId) {
        this.retailsByPointId = retailsByPointId;
    }

    @OneToMany()
    @JoinColumn(name="POI_PointID")
    public Collection<SocialEntity> getSocialsByPointId() {
        return socialsByPointId;
    }

    public void setSocialsByPointId(Collection<SocialEntity> socialsByPointId) {
        this.socialsByPointId = socialsByPointId;
    }

    @OneToMany()
    @JoinColumn(name="POI_PointID")
    public Collection<TransportationEntity> getTransportationsByPointId() {
        return transportationsByPointId;
    }

    public void setTransportationsByPointId(Collection<TransportationEntity> transportationsByPointId) {
        this.transportationsByPointId = transportationsByPointId;
    }
}
