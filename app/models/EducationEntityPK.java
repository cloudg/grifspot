package models;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by tony on 12/6/14.
 */
public class EducationEntityPK implements Serializable {
    private int educationId;
    private int poiPointId;

    @Column(name = "EducationID")
    @Id
    public int getEducationId() {
        return educationId;
    }

    public void setEducationId(int educationId) {
        this.educationId = educationId;
    }

    @Column(name = "POI_PointID")
    @Id
    public int getPoiPointId() {
        return poiPointId;
    }

    public void setPoiPointId(int poiPointId) {
        this.poiPointId = poiPointId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EducationEntityPK that = (EducationEntityPK) o;

        if (educationId != that.educationId) return false;
        if (poiPointId != that.poiPointId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = educationId;
        result = 31 * result + poiPointId;
        return result;
    }
}
