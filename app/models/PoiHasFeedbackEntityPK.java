package models;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by tony on 12/6/14.
 */
public class PoiHasFeedbackEntityPK implements Serializable {
    private int poiPointId;
    private int feedbackFeedbackId;

    @Column(name = "POI_PointID")
    @Id
    public int getPoiPointId() {
        return poiPointId;
    }

    public void setPoiPointId(int poiPointId) {
        this.poiPointId = poiPointId;
    }

    @Column(name = "Feedback_FeedbackID")
    @Id
    public int getFeedbackFeedbackId() {
        return feedbackFeedbackId;
    }

    public void setFeedbackFeedbackId(int feedbackFeedbackId) {
        this.feedbackFeedbackId = feedbackFeedbackId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PoiHasFeedbackEntityPK that = (PoiHasFeedbackEntityPK) o;

        if (feedbackFeedbackId != that.feedbackFeedbackId) return false;
        if (poiPointId != that.poiPointId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = poiPointId;
        result = 31 * result + feedbackFeedbackId;
        return result;
    }
}
