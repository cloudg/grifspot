package models;

import javax.persistence.*;
import java.util.Arrays;

/**
 * Created by tony on 12/6/14.
 */
@Entity
@Table(name = "Retail", schema = "", catalog = "GrifSpot")
@IdClass(RetailEntityPK.class)
public class RetailEntity {
    private int retailId;
    private int poiPointId;
    private String retType;
    private byte[] localBusiness;
    private byte[] used;

    @Id
    @Column(name = "RetailID")
    public int getRetailId() {
        return retailId;
    }

    public void setRetailId(int retailId) {
        this.retailId = retailId;
    }

    @Id
    @Column(name = "POI_PointID")
    public int getPoiPointId() {
        return poiPointId;
    }

    public void setPoiPointId(int poiPointId) {
        this.poiPointId = poiPointId;
    }

    @Basic
    @Column(name = "RetType")
    public String getRetType() {
        return retType;
    }

    public void setRetType(String retType) {
        this.retType = retType;
    }

    @Basic
    @Column(name = "LocalBusiness")
    public byte[] getLocalBusiness() {
        return localBusiness;
    }

    public void setLocalBusiness(byte[] localBusiness) {
        this.localBusiness = localBusiness;
    }

    @Basic
    @Column(name = "Used")
    public byte[] getUsed() {
        return used;
    }

    public void setUsed(byte[] used) {
        this.used = used;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RetailEntity that = (RetailEntity) o;

        if (poiPointId != that.poiPointId) return false;
        if (retailId != that.retailId) return false;
        if (!Arrays.equals(localBusiness, that.localBusiness)) return false;
        if (retType != null ? !retType.equals(that.retType) : that.retType != null) return false;
        if (!Arrays.equals(used, that.used)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = retailId;
        result = 31 * result + poiPointId;
        result = 31 * result + (retType != null ? retType.hashCode() : 0);
        result = 31 * result + (localBusiness != null ? Arrays.hashCode(localBusiness) : 0);
        result = 31 * result + (used != null ? Arrays.hashCode(used) : 0);
        return result;
    }
}
