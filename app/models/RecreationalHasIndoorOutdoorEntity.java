package models;

import javax.persistence.*;

/**
 * Created by tony on 12/6/14.
 */
@Entity
@Table(name = "Recreational_has_IndoorOutdoor", schema = "", catalog = "GrifSpot")
@IdClass(RecreationalHasIndoorOutdoorEntityPK.class)
public class RecreationalHasIndoorOutdoorEntity {
    private int recreationalRecreationalId;
    private int indoorOutdoorIndoorOutdoorId;

    @Id
    @Column(name = "Recreational_RecreationalID")
    public int getRecreationalRecreationalId() {
        return recreationalRecreationalId;
    }

    public void setRecreationalRecreationalId(int recreationalRecreationalId) {
        this.recreationalRecreationalId = recreationalRecreationalId;
    }

    @Id
    @Column(name = "IndoorOutdoor_IndoorOutdoorID")
    public int getIndoorOutdoorIndoorOutdoorId() {
        return indoorOutdoorIndoorOutdoorId;
    }

    public void setIndoorOutdoorIndoorOutdoorId(int indoorOutdoorIndoorOutdoorId) {
        this.indoorOutdoorIndoorOutdoorId = indoorOutdoorIndoorOutdoorId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RecreationalHasIndoorOutdoorEntity that = (RecreationalHasIndoorOutdoorEntity) o;

        if (indoorOutdoorIndoorOutdoorId != that.indoorOutdoorIndoorOutdoorId) return false;
        if (recreationalRecreationalId != that.recreationalRecreationalId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = recreationalRecreationalId;
        result = 31 * result + indoorOutdoorIndoorOutdoorId;
        return result;
    }
}
