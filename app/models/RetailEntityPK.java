package models;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by tony on 12/6/14.
 */
public class RetailEntityPK implements Serializable {
    private int retailId;
    private int poiPointId;

    @Column(name = "RetailID")
    @Id
    public int getRetailId() {
        return retailId;
    }

    public void setRetailId(int retailId) {
        this.retailId = retailId;
    }

    @Column(name = "POI_PointID")
    @Id
    public int getPoiPointId() {
        return poiPointId;
    }

    public void setPoiPointId(int poiPointId) {
        this.poiPointId = poiPointId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RetailEntityPK that = (RetailEntityPK) o;

        if (poiPointId != that.poiPointId) return false;
        if (retailId != that.retailId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = retailId;
        result = 31 * result + poiPointId;
        return result;
    }
}
