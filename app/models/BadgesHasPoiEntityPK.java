package models;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by tony on 12/6/14.
 */
public class BadgesHasPoiEntityPK implements Serializable {
    private int badgesBadgesId;
    private int poiPointId;

    @Column(name = "Badges_BadgesID")
    @Id
    public int getBadgesBadgesId() {
        return badgesBadgesId;
    }

    public void setBadgesBadgesId(int badgesBadgesId) {
        this.badgesBadgesId = badgesBadgesId;
    }

    @Column(name = "POI_PointID")
    @Id
    public int getPoiPointId() {
        return poiPointId;
    }

    public void setPoiPointId(int poiPointId) {
        this.poiPointId = poiPointId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BadgesHasPoiEntityPK that = (BadgesHasPoiEntityPK) o;

        if (badgesBadgesId != that.badgesBadgesId) return false;
        if (poiPointId != that.poiPointId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = badgesBadgesId;
        result = 31 * result + poiPointId;
        return result;
    }
}
