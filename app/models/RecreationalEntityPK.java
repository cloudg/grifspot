package models;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by tony on 12/6/14.
 */
public class RecreationalEntityPK implements Serializable {
    private int recreationalId;
    private int poiPointId;

    @Column(name = "RecreationalID")
    @Id
    public int getRecreationalId() {
        return recreationalId;
    }

    public void setRecreationalId(int recreationalId) {
        this.recreationalId = recreationalId;
    }

    @Column(name = "POI_PointID")
    @Id
    public int getPoiPointId() {
        return poiPointId;
    }

    public void setPoiPointId(int poiPointId) {
        this.poiPointId = poiPointId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RecreationalEntityPK that = (RecreationalEntityPK) o;

        if (poiPointId != that.poiPointId) return false;
        if (recreationalId != that.recreationalId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = recreationalId;
        result = 31 * result + poiPointId;
        return result;
    }
}
