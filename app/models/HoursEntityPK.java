package models;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by tony on 12/6/14.
 */
public class HoursEntityPK implements Serializable {
    private int hoursId;
    private int poiPointId;

    @Column(name = "HoursID")
    @Id
    public int getHoursId() {
        return hoursId;
    }

    public void setHoursId(int hoursId) {
        this.hoursId = hoursId;
    }

    @Column(name = "POI_PointID")
    @Id
    public int getPoiPointId() {
        return poiPointId;
    }

    public void setPoiPointId(int poiPointId) {
        this.poiPointId = poiPointId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HoursEntityPK that = (HoursEntityPK) o;

        if (hoursId != that.hoursId) return false;
        if (poiPointId != that.poiPointId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = hoursId;
        result = 31 * result + poiPointId;
        return result;
    }
}
