package models;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by tony on 12/6/14.
 */
public class TransportationEntityPK implements Serializable {
    private int ptid;
    private int poiPointId;

    @Column(name = "PTID")
    @Id
    public int getPtid() {
        return ptid;
    }

    public void setPtid(int ptid) {
        this.ptid = ptid;
    }

    @Column(name = "POI_PointID")
    @Id
    public int getPoiPointId() {
        return poiPointId;
    }

    public void setPoiPointId(int poiPointId) {
        this.poiPointId = poiPointId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TransportationEntityPK that = (TransportationEntityPK) o;

        if (poiPointId != that.poiPointId) return false;
        if (ptid != that.ptid) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = ptid;
        result = 31 * result + poiPointId;
        return result;
    }
}
