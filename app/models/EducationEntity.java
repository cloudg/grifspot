package models;

import javax.persistence.*;

/**
 * Created by tony on 12/6/14.
 */
@Entity
@Table(name = "Education", schema = "", catalog = "GrifSpot")
@IdClass(EducationEntityPK.class)
public class EducationEntity {
    private int educationId;
    private int poiPointId;
    private String eduType;
    private String curriculum;
    private String calendar;

    @Id
    @Column(name = "EducationID")
    public int getEducationId() {
        return educationId;
    }

    public void setEducationId(int educationId) {
        this.educationId = educationId;
    }

    @Id
    @Column(name = "POI_PointID")
    public int getPoiPointId() {
        return poiPointId;
    }

    public void setPoiPointId(int poiPointId) {
        this.poiPointId = poiPointId;
    }

    @Basic
    @Column(name = "EduType")
    public String getEduType() {
        return eduType;
    }

    public void setEduType(String eduType) {
        this.eduType = eduType;
    }

    @Basic
    @Column(name = "Curriculum")
    public String getCurriculum() {
        return curriculum;
    }

    public void setCurriculum(String curriculum) {
        this.curriculum = curriculum;
    }

    @Basic
    @Column(name = "Calendar")
    public String getCalendar() {
        return calendar;
    }

    public void setCalendar(String calendar) {
        this.calendar = calendar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EducationEntity that = (EducationEntity) o;

        if (educationId != that.educationId) return false;
        if (poiPointId != that.poiPointId) return false;
        if (calendar != null ? !calendar.equals(that.calendar) : that.calendar != null) return false;
        if (curriculum != null ? !curriculum.equals(that.curriculum) : that.curriculum != null) return false;
        if (eduType != null ? !eduType.equals(that.eduType) : that.eduType != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = educationId;
        result = 31 * result + poiPointId;
        result = 31 * result + (eduType != null ? eduType.hashCode() : 0);
        result = 31 * result + (curriculum != null ? curriculum.hashCode() : 0);
        result = 31 * result + (calendar != null ? calendar.hashCode() : 0);
        return result;
    }
}
