package models;

import javax.persistence.*;
import java.util.Arrays;

/**
 * Created by tony on 12/6/14.
 */
@Entity
@Table(name = "Feedback", schema = "", catalog = "GrifSpot")
@IdClass(FeedbackEntityPK.class)
public class FeedbackEntity {
    private int feedbackId;
    private int questionNum;
    private byte[] ymn;

    @Id
    @Column(name = "FeedbackID")
    public int getFeedbackId() {
        return feedbackId;
    }

    public void setFeedbackId(int feedbackId) {
        this.feedbackId = feedbackId;
    }

    @Id
    @Column(name = "QuestionNum")
    public int getQuestionNum() {
        return questionNum;
    }

    public void setQuestionNum(int questionNum) {
        this.questionNum = questionNum;
    }

    @Basic
    @Column(name = "YMN")
    public byte[] getYmn() {
        return ymn;
    }

    public void setYmn(byte[] ymn) {
        this.ymn = ymn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FeedbackEntity that = (FeedbackEntity) o;

        if (feedbackId != that.feedbackId) return false;
        if (questionNum != that.questionNum) return false;
        if (!Arrays.equals(ymn, that.ymn)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = feedbackId;
        result = 31 * result + questionNum;
        result = 31 * result + (ymn != null ? Arrays.hashCode(ymn) : 0);
        return result;
    }
}
