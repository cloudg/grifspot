package models;

import javax.persistence.*;

/**
 * Created by tony on 12/6/14.
 */
@Entity
@Table(name = "Architecture", schema = "", catalog = "GrifSpot")
@IdClass(ArchitectureEntityPK.class)
public class ArchitectureEntity {
    private int architectureId;
    private int poiPointId;
    private String archType;
    private String purpose;

    @Id
    @Column(name = "ArchitectureID")
    public int getArchitectureId() {
        return architectureId;
    }

    public void setArchitectureId(int architectureId) {
        this.architectureId = architectureId;
    }

    @Id
    @Column(name = "POI_PointID")
    public int getPoiPointId() {
        return poiPointId;
    }

    public void setPoiPointId(int poiPointId) {
        this.poiPointId = poiPointId;
    }

    @Basic
    @Column(name = "ArchType")
    public String getArchType() {
        return archType;
    }

    public void setArchType(String archType) {
        this.archType = archType;
    }

    @Basic
    @Column(name = "Purpose")
    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArchitectureEntity that = (ArchitectureEntity) o;

        if (architectureId != that.architectureId) return false;
        if (poiPointId != that.poiPointId) return false;
        if (archType != null ? !archType.equals(that.archType) : that.archType != null) return false;
        if (purpose != null ? !purpose.equals(that.purpose) : that.purpose != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = architectureId;
        result = 31 * result + poiPointId;
        result = 31 * result + (archType != null ? archType.hashCode() : 0);
        result = 31 * result + (purpose != null ? purpose.hashCode() : 0);
        return result;
    }
}
