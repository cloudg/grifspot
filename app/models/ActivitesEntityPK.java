package models;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by tony on 12/6/14.
 */
public class ActivitesEntityPK implements Serializable {
    private int activitesId;
    private int educationEducationId;

    @Column(name = "ActivitesID")
    @Id
    public int getActivitesId() {
        return activitesId;
    }

    public void setActivitesId(int activitesId) {
        this.activitesId = activitesId;
    }

    @Column(name = "Education_EducationID")
    @Id
    public int getEducationEducationId() {
        return educationEducationId;
    }

    public void setEducationEducationId(int educationEducationId) {
        this.educationEducationId = educationEducationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ActivitesEntityPK that = (ActivitesEntityPK) o;

        if (activitesId != that.activitesId) return false;
        if (educationEducationId != that.educationEducationId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = activitesId;
        result = 31 * result + educationEducationId;
        return result;
    }
}
