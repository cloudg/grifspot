package models;

import javax.persistence.*;

/**
 * Created by tony on 12/6/14.
 */
@Entity
@Table(name = "Activites", schema = "", catalog = "GrifSpot")
@IdClass(ActivitesEntityPK.class)
public class ActivitesEntity {
    private int activitesId;
    private int educationEducationId;
    private String name;

    @Id
    @Column(name = "ActivitesID")
    public int getActivitesId() {
        return activitesId;
    }

    public void setActivitesId(int activitesId) {
        this.activitesId = activitesId;
    }

    @Id
    @Column(name = "Education_EducationID")
    public int getEducationEducationId() {
        return educationEducationId;
    }

    public void setEducationEducationId(int educationEducationId) {
        this.educationEducationId = educationEducationId;
    }

    @Basic
    @Column(name = "Name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ActivitesEntity that = (ActivitesEntity) o;

        if (activitesId != that.activitesId) return false;
        if (educationEducationId != that.educationEducationId) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = activitesId;
        result = 31 * result + educationEducationId;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
