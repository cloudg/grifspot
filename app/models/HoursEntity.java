package models;

import javax.persistence.*;

/**
 * Created by tony on 12/6/14.
 */
@Entity
@Table(name = "Hours", schema = "", catalog = "GrifSpot")
@IdClass(HoursEntityPK.class)
public class HoursEntity {
    private int hoursId;
    private int poiPointId;
    private String day;
    private String openTime;
    private String closeTime;

    @Id
    @Column(name = "HoursID")
    public int getHoursId() {
        return hoursId;
    }

    public void setHoursId(int hoursId) {
        this.hoursId = hoursId;
    }

    @Id
    @Column(name = "POI_PointID")
    public int getPoiPointId() {
        return poiPointId;
    }

    public void setPoiPointId(int poiPointId) {
        this.poiPointId = poiPointId;
    }

    @Basic
    @Column(name = "Day")
    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    @Basic
    @Column(name = "OpenTime")
    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    @Basic
    @Column(name = "CloseTime")
    public String getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HoursEntity that = (HoursEntity) o;

        if (hoursId != that.hoursId) return false;
        if (poiPointId != that.poiPointId) return false;
        if (closeTime != null ? !closeTime.equals(that.closeTime) : that.closeTime != null) return false;
        if (day != null ? !day.equals(that.day) : that.day != null) return false;
        if (openTime != null ? !openTime.equals(that.openTime) : that.openTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = hoursId;
        result = 31 * result + poiPointId;
        result = 31 * result + (day != null ? day.hashCode() : 0);
        result = 31 * result + (openTime != null ? openTime.hashCode() : 0);
        result = 31 * result + (closeTime != null ? closeTime.hashCode() : 0);
        return result;
    }
}
