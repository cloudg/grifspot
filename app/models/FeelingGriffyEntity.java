package models;

import javax.persistence.*;

/**
 * Created by tony on 12/6/14.
 */
@Entity
@Table(name = "FeelingGriffy", schema = "", catalog = "GrifSpot")
public class FeelingGriffyEntity {
    private int feelingGriffyId;
    private int transportationPtid;
    private int socialSocialId;
    private int recreationalRecreationalId;
    private int architectureArchitectureId;
    private int retailRetailId;
    private int educationEducationId;
    private int restaurantRestaurantId;

    @Id
    @Column(name = "FeelingGriffyID")
    public int getFeelingGriffyId() {
        return feelingGriffyId;
    }

    public void setFeelingGriffyId(int feelingGriffyId) {
        this.feelingGriffyId = feelingGriffyId;
    }

    @Basic
    @Column(name = "Transportation_PTID")
    public int getTransportationPtid() {
        return transportationPtid;
    }

    public void setTransportationPtid(int transportationPtid) {
        this.transportationPtid = transportationPtid;
    }

    @Basic
    @Column(name = "Social_SocialID")
    public int getSocialSocialId() {
        return socialSocialId;
    }

    public void setSocialSocialId(int socialSocialId) {
        this.socialSocialId = socialSocialId;
    }

    @Basic
    @Column(name = "Recreational_RecreationalID")
    public int getRecreationalRecreationalId() {
        return recreationalRecreationalId;
    }

    public void setRecreationalRecreationalId(int recreationalRecreationalId) {
        this.recreationalRecreationalId = recreationalRecreationalId;
    }

    @Basic
    @Column(name = "Architecture_ArchitectureID")
    public int getArchitectureArchitectureId() {
        return architectureArchitectureId;
    }

    public void setArchitectureArchitectureId(int architectureArchitectureId) {
        this.architectureArchitectureId = architectureArchitectureId;
    }

    @Basic
    @Column(name = "Retail_RetailID")
    public int getRetailRetailId() {
        return retailRetailId;
    }

    public void setRetailRetailId(int retailRetailId) {
        this.retailRetailId = retailRetailId;
    }

    @Basic
    @Column(name = "Education_EducationID")
    public int getEducationEducationId() {
        return educationEducationId;
    }

    public void setEducationEducationId(int educationEducationId) {
        this.educationEducationId = educationEducationId;
    }

    @Basic
    @Column(name = "Restaurant_RestaurantID")
    public int getRestaurantRestaurantId() {
        return restaurantRestaurantId;
    }

    public void setRestaurantRestaurantId(int restaurantRestaurantId) {
        this.restaurantRestaurantId = restaurantRestaurantId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FeelingGriffyEntity that = (FeelingGriffyEntity) o;

        if (architectureArchitectureId != that.architectureArchitectureId) return false;
        if (educationEducationId != that.educationEducationId) return false;
        if (feelingGriffyId != that.feelingGriffyId) return false;
        if (recreationalRecreationalId != that.recreationalRecreationalId) return false;
        if (restaurantRestaurantId != that.restaurantRestaurantId) return false;
        if (retailRetailId != that.retailRetailId) return false;
        if (socialSocialId != that.socialSocialId) return false;
        if (transportationPtid != that.transportationPtid) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = feelingGriffyId;
        result = 31 * result + transportationPtid;
        result = 31 * result + socialSocialId;
        result = 31 * result + recreationalRecreationalId;
        result = 31 * result + architectureArchitectureId;
        result = 31 * result + retailRetailId;
        result = 31 * result + educationEducationId;
        result = 31 * result + restaurantRestaurantId;
        return result;
    }
}
