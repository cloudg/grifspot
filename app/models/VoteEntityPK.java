package models;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by tony on 12/6/14.
 */
public class VoteEntityPK implements Serializable {
    private int voteId;
    private int comentsCommentId;
    private int userUserId;

    @Column(name = "VoteID")
    @Id
    public int getVoteId() {
        return voteId;
    }

    public void setVoteId(int voteId) {
        this.voteId = voteId;
    }

    @Column(name = "Coments_CommentID")
    @Id
    public int getComentsCommentId() {
        return comentsCommentId;
    }

    public void setComentsCommentId(int comentsCommentId) {
        this.comentsCommentId = comentsCommentId;
    }

    @Column(name = "User_UserID")
    @Id
    public int getUserUserId() {
        return userUserId;
    }

    public void setUserUserId(int userUserId) {
        this.userUserId = userUserId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VoteEntityPK that = (VoteEntityPK) o;

        if (comentsCommentId != that.comentsCommentId) return false;
        if (userUserId != that.userUserId) return false;
        if (voteId != that.voteId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = voteId;
        result = 31 * result + comentsCommentId;
        result = 31 * result + userUserId;
        return result;
    }
}
