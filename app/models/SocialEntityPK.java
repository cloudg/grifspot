package models;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by tony on 12/6/14.
 */
public class SocialEntityPK implements Serializable {
    private int socialId;
    private int poiPointId;

    @Column(name = "SocialID")
    @Id
    public int getSocialId() {
        return socialId;
    }

    public void setSocialId(int socialId) {
        this.socialId = socialId;
    }

    @Column(name = "POI_PointID")
    @Id
    public int getPoiPointId() {
        return poiPointId;
    }

    public void setPoiPointId(int poiPointId) {
        this.poiPointId = poiPointId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SocialEntityPK that = (SocialEntityPK) o;

        if (poiPointId != that.poiPointId) return false;
        if (socialId != that.socialId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = socialId;
        result = 31 * result + poiPointId;
        return result;
    }
}
