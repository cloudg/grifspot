package models;

import javax.persistence.*;

/**
 * Created by tony on 12/6/14.
 */
@Entity
@Table(name = "IndoorOutdoor", schema = "", catalog = "GrifSpot")
public class IndoorOutdoorEntity {
    private int indoorOutdoorId;
    private String type;

    @Id
    @Column(name = "IndoorOutdoorID")
    public int getIndoorOutdoorId() {
        return indoorOutdoorId;
    }

    public void setIndoorOutdoorId(int indoorOutdoorId) {
        this.indoorOutdoorId = indoorOutdoorId;
    }

    @Basic
    @Column(name = "Type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IndoorOutdoorEntity that = (IndoorOutdoorEntity) o;

        if (indoorOutdoorId != that.indoorOutdoorId) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = indoorOutdoorId;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }
}
