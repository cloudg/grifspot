package models;

import javax.persistence.*;

/**
 * Created by tony on 12/6/14.
 */
@Entity
@Table(name = "Recreational_has_Equipment", schema = "", catalog = "GrifSpot")
@IdClass(RecreationalHasEquipmentEntityPK.class)
public class RecreationalHasEquipmentEntity {
    private int recreationalRecreationalId;
    private int equipmentEquipmentId;

    @Id
    @Column(name = "Recreational_RecreationalID")
    public int getRecreationalRecreationalId() {
        return recreationalRecreationalId;
    }

    public void setRecreationalRecreationalId(int recreationalRecreationalId) {
        this.recreationalRecreationalId = recreationalRecreationalId;
    }

    @Id
    @Column(name = "Equipment_EquipmentID")
    public int getEquipmentEquipmentId() {
        return equipmentEquipmentId;
    }

    public void setEquipmentEquipmentId(int equipmentEquipmentId) {
        this.equipmentEquipmentId = equipmentEquipmentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RecreationalHasEquipmentEntity that = (RecreationalHasEquipmentEntity) o;

        if (equipmentEquipmentId != that.equipmentEquipmentId) return false;
        if (recreationalRecreationalId != that.recreationalRecreationalId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = recreationalRecreationalId;
        result = 31 * result + equipmentEquipmentId;
        return result;
    }
}
