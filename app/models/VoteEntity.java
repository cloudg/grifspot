package models;

import javax.persistence.*;
import java.util.Arrays;

/**
 * Created by tony on 12/6/14.
 */
@Entity
@Table(name = "Vote", schema = "", catalog = "GrifSpot")
@IdClass(VoteEntityPK.class)
public class VoteEntity {
    private int voteId;
    private int comentsCommentId;
    private int userUserId;
    private byte[] thumbUp;

    @Id
    @Column(name = "VoteID")
    public int getVoteId() {
        return voteId;
    }

    public void setVoteId(int voteId) {
        this.voteId = voteId;
    }

    @Id
    @Column(name = "Coments_CommentID")
    public int getComentsCommentId() {
        return comentsCommentId;
    }

    public void setComentsCommentId(int comentsCommentId) {
        this.comentsCommentId = comentsCommentId;
    }

    @Id
    @Column(name = "User_UserID")
    public int getUserUserId() {
        return userUserId;
    }

    public void setUserUserId(int userUserId) {
        this.userUserId = userUserId;
    }

    @Basic
    @Column(name = "ThumbUp")
    public byte[] getThumbUp() {
        return thumbUp;
    }

    public void setThumbUp(byte[] thumbUp) {
        this.thumbUp = thumbUp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VoteEntity that = (VoteEntity) o;

        if (comentsCommentId != that.comentsCommentId) return false;
        if (userUserId != that.userUserId) return false;
        if (voteId != that.voteId) return false;
        if (!Arrays.equals(thumbUp, that.thumbUp)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = voteId;
        result = 31 * result + comentsCommentId;
        result = 31 * result + userUserId;
        result = 31 * result + (thumbUp != null ? Arrays.hashCode(thumbUp) : 0);
        return result;
    }
}
