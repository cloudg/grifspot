package models;

import javax.persistence.*;

/**
 * Created by tony on 12/6/14.
 */
@Entity
@Table(name = "Season", schema = "", catalog = "GrifSpot")
public class SeasonEntity {
    private int seasonId;
    private String season;

    @Id
    @Column(name = "SeasonID")
    public int getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(int seasonId) {
        this.seasonId = seasonId;
    }

    @Basic
    @Column(name = "Season")
    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SeasonEntity that = (SeasonEntity) o;

        if (seasonId != that.seasonId) return false;
        if (season != null ? !season.equals(that.season) : that.season != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = seasonId;
        result = 31 * result + (season != null ? season.hashCode() : 0);
        return result;
    }
}
