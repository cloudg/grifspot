package models;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by tony on 12/6/14.
 */
public class RecreationalHasIndoorOutdoorEntityPK implements Serializable {
    private int recreationalRecreationalId;
    private int indoorOutdoorIndoorOutdoorId;

    @Column(name = "Recreational_RecreationalID")
    @Id
    public int getRecreationalRecreationalId() {
        return recreationalRecreationalId;
    }

    public void setRecreationalRecreationalId(int recreationalRecreationalId) {
        this.recreationalRecreationalId = recreationalRecreationalId;
    }

    @Column(name = "IndoorOutdoor_IndoorOutdoorID")
    @Id
    public int getIndoorOutdoorIndoorOutdoorId() {
        return indoorOutdoorIndoorOutdoorId;
    }

    public void setIndoorOutdoorIndoorOutdoorId(int indoorOutdoorIndoorOutdoorId) {
        this.indoorOutdoorIndoorOutdoorId = indoorOutdoorIndoorOutdoorId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RecreationalHasIndoorOutdoorEntityPK that = (RecreationalHasIndoorOutdoorEntityPK) o;

        if (indoorOutdoorIndoorOutdoorId != that.indoorOutdoorIndoorOutdoorId) return false;
        if (recreationalRecreationalId != that.recreationalRecreationalId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = recreationalRecreationalId;
        result = 31 * result + indoorOutdoorIndoorOutdoorId;
        return result;
    }
}
