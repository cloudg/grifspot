package models;

import javax.persistence.*;
import java.util.Arrays;

/**
 * Created by tony on 12/6/14.
 */
@Entity
@Table(name = "Social", schema = "", catalog = "GrifSpot")
@IdClass(SocialEntityPK.class)
public class SocialEntity {
    private int socialId;
    private int poiPointId;
    private int minAge;
    private byte[] smoking;
    private byte[] lgbtFriendly;
    private Integer occupency;

    @Id
    @Column(name = "SocialID")
    public int getSocialId() {
        return socialId;
    }

    public void setSocialId(int socialId) {
        this.socialId = socialId;
    }

    @Id
    @Column(name = "POI_PointID")
    public int getPoiPointId() {
        return poiPointId;
    }

    public void setPoiPointId(int poiPointId) {
        this.poiPointId = poiPointId;
    }

    @Basic
    @Column(name = "MinAge")
    public int getMinAge() {
        return minAge;
    }

    public void setMinAge(int minAge) {
        this.minAge = minAge;
    }

    @Basic
    @Column(name = "Smoking")
    public byte[] getSmoking() {
        return smoking;
    }

    public void setSmoking(byte[] smoking) {
        this.smoking = smoking;
    }

    @Basic
    @Column(name = "LGBT_Friendly")
    public byte[] getLgbtFriendly() {
        return lgbtFriendly;
    }

    public void setLgbtFriendly(byte[] lgbtFriendly) {
        this.lgbtFriendly = lgbtFriendly;
    }

    @Basic
    @Column(name = "Occupency")
    public Integer getOccupency() {
        return occupency;
    }

    public void setOccupency(Integer occupency) {
        this.occupency = occupency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SocialEntity that = (SocialEntity) o;

        if (minAge != that.minAge) return false;
        if (poiPointId != that.poiPointId) return false;
        if (socialId != that.socialId) return false;
        if (!Arrays.equals(lgbtFriendly, that.lgbtFriendly)) return false;
        if (occupency != null ? !occupency.equals(that.occupency) : that.occupency != null) return false;
        if (!Arrays.equals(smoking, that.smoking)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = socialId;
        result = 31 * result + poiPointId;
        result = 31 * result + minAge;
        result = 31 * result + (smoking != null ? Arrays.hashCode(smoking) : 0);
        result = 31 * result + (lgbtFriendly != null ? Arrays.hashCode(lgbtFriendly) : 0);
        result = 31 * result + (occupency != null ? occupency.hashCode() : 0);
        return result;
    }
}
