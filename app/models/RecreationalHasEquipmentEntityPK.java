package models;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by tony on 12/6/14.
 */
public class RecreationalHasEquipmentEntityPK implements Serializable {
    private int recreationalRecreationalId;
    private int equipmentEquipmentId;

    @Column(name = "Recreational_RecreationalID")
    @Id
    public int getRecreationalRecreationalId() {
        return recreationalRecreationalId;
    }

    public void setRecreationalRecreationalId(int recreationalRecreationalId) {
        this.recreationalRecreationalId = recreationalRecreationalId;
    }

    @Column(name = "Equipment_EquipmentID")
    @Id
    public int getEquipmentEquipmentId() {
        return equipmentEquipmentId;
    }

    public void setEquipmentEquipmentId(int equipmentEquipmentId) {
        this.equipmentEquipmentId = equipmentEquipmentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RecreationalHasEquipmentEntityPK that = (RecreationalHasEquipmentEntityPK) o;

        if (equipmentEquipmentId != that.equipmentEquipmentId) return false;
        if (recreationalRecreationalId != that.recreationalRecreationalId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = recreationalRecreationalId;
        result = 31 * result + equipmentEquipmentId;
        return result;
    }
}
