package models;

import javax.persistence.*;

/**
 * Created by tony on 12/6/14.
 */
@Entity
@Table(name = "Recreational", schema = "", catalog = "GrifSpot")
@IdClass(RecreationalEntityPK.class)
public class RecreationalEntity {
    private int recreationalId;
    private int poiPointId;
    private String services;

    @Id
    @Column(name = "RecreationalID")
    public int getRecreationalId() {
        return recreationalId;
    }

    public void setRecreationalId(int recreationalId) {
        this.recreationalId = recreationalId;
    }

    @Id
    @Column(name = "POI_PointID")
    public int getPoiPointId() {
        return poiPointId;
    }

    public void setPoiPointId(int poiPointId) {
        this.poiPointId = poiPointId;
    }

    @Basic
    @Column(name = "Services")
    public String getServices() {
        return services;
    }

    public void setServices(String services) {
        this.services = services;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RecreationalEntity that = (RecreationalEntity) o;

        if (poiPointId != that.poiPointId) return false;
        if (recreationalId != that.recreationalId) return false;
        if (services != null ? !services.equals(that.services) : that.services != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = recreationalId;
        result = 31 * result + poiPointId;
        result = 31 * result + (services != null ? services.hashCode() : 0);
        return result;
    }
}
