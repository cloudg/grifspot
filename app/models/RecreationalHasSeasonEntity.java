package models;

import javax.persistence.*;

/**
 * Created by tony on 12/6/14.
 */
@Entity
@Table(name = "Recreational_has_Season", schema = "", catalog = "GrifSpot")
@IdClass(RecreationalHasSeasonEntityPK.class)
public class RecreationalHasSeasonEntity {
    private int recreationalRecreationalId;
    private int seasonSeasonId;

    @Id
    @Column(name = "Recreational_RecreationalID")
    public int getRecreationalRecreationalId() {
        return recreationalRecreationalId;
    }

    public void setRecreationalRecreationalId(int recreationalRecreationalId) {
        this.recreationalRecreationalId = recreationalRecreationalId;
    }

    @Id
    @Column(name = "Season_SeasonID")
    public int getSeasonSeasonId() {
        return seasonSeasonId;
    }

    public void setSeasonSeasonId(int seasonSeasonId) {
        this.seasonSeasonId = seasonSeasonId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RecreationalHasSeasonEntity that = (RecreationalHasSeasonEntity) o;

        if (recreationalRecreationalId != that.recreationalRecreationalId) return false;
        if (seasonSeasonId != that.seasonSeasonId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = recreationalRecreationalId;
        result = 31 * result + seasonSeasonId;
        return result;
    }
}
