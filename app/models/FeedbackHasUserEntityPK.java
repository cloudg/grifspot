package models;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by tony on 12/6/14.
 */
public class FeedbackHasUserEntityPK implements Serializable {
    private int feedbackFeedbackId;
    private int userUserId;

    @Column(name = "Feedback_FeedbackID")
    @Id
    public int getFeedbackFeedbackId() {
        return feedbackFeedbackId;
    }

    public void setFeedbackFeedbackId(int feedbackFeedbackId) {
        this.feedbackFeedbackId = feedbackFeedbackId;
    }

    @Column(name = "User_UserID")
    @Id
    public int getUserUserId() {
        return userUserId;
    }

    public void setUserUserId(int userUserId) {
        this.userUserId = userUserId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FeedbackHasUserEntityPK that = (FeedbackHasUserEntityPK) o;

        if (feedbackFeedbackId != that.feedbackFeedbackId) return false;
        if (userUserId != that.userUserId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = feedbackFeedbackId;
        result = 31 * result + userUserId;
        return result;
    }
}
