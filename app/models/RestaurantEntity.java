package models;

import javax.persistence.*;

/**
 * Created by tony on 12/6/14.
 */
@Entity
@Table(name = "Restaurant", schema = "", catalog = "GrifSpot")
public class RestaurantEntity {
    private int restaurantId;
    private Integer poiPointId;
    private String menu;
    private String restType;
    private String phone;
    private String specialty;
    private String zagatRating;
    private String style;
    private String price;
    private boolean local;

    @Id
    @Column(name = "RestaurantID")
    public int getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(int restaurantId) {
        this.restaurantId = restaurantId;
    }

    @Basic
    @Column(name = "POI_PointID")
    public Integer getPoiPointId() {
        return poiPointId;
    }

    public void setPoiPointId(Integer poiPointId) {
        this.poiPointId = poiPointId;
    }

    @Basic
    @Column(name = "Menu")
    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    @Basic
    @Column(name = "RestType")
    public String getRestType() {
        return restType;
    }

    public void setRestType(String restType) {
        this.restType = restType;
    }

    @Basic
    @Column(name = "Phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Basic
    @Column(name = "Specialty")
    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    @Basic
    @Column(name = "ZagatRating")
    public String getZagatRating() {
        return zagatRating;
    }

    public void setZagatRating(String zagatRating) {
        this.zagatRating = zagatRating;
    }

    @Basic
    @Column(name = "Style")
    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    @Basic
    @Column(name = "Price")
    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Basic
    @Column(name = "Local")
    public boolean isLocal() {
        return local;
    }

    public void setLocal(boolean local) {
        this.local = local;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RestaurantEntity that = (RestaurantEntity) o;

        if (local != that.local) return false;
        if (restaurantId != that.restaurantId) return false;
        if (menu != null ? !menu.equals(that.menu) : that.menu != null) return false;
        if (phone != null ? !phone.equals(that.phone) : that.phone != null) return false;
        if (poiPointId != null ? !poiPointId.equals(that.poiPointId) : that.poiPointId != null) return false;
        if (price != null ? !price.equals(that.price) : that.price != null) return false;
        if (restType != null ? !restType.equals(that.restType) : that.restType != null) return false;
        if (specialty != null ? !specialty.equals(that.specialty) : that.specialty != null) return false;
        if (style != null ? !style.equals(that.style) : that.style != null) return false;
        if (zagatRating != null ? !zagatRating.equals(that.zagatRating) : that.zagatRating != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = restaurantId;
        result = 31 * result + (poiPointId != null ? poiPointId.hashCode() : 0);
        result = 31 * result + (menu != null ? menu.hashCode() : 0);
        result = 31 * result + (restType != null ? restType.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        result = 31 * result + (specialty != null ? specialty.hashCode() : 0);
        result = 31 * result + (zagatRating != null ? zagatRating.hashCode() : 0);
        result = 31 * result + (style != null ? style.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (local ? 1 : 0);
        return result;
    }
}
