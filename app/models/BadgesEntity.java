package models;

import javax.persistence.*;

/**
 * Created by tony on 12/6/14.
 */
@Entity
@Table(name = "Badges", schema = "", catalog = "GrifSpot")
public class BadgesEntity {
    private int badgesId;
    private String badgesJpg;

    @Id
    @Column(name = "BadgesID")
    public int getBadgesId() {
        return badgesId;
    }

    public void setBadgesId(int badgesId) {
        this.badgesId = badgesId;
    }

    @Basic
    @Column(name = "BadgesJPG")
    public String getBadgesJpg() {
        return badgesJpg;
    }

    public void setBadgesJpg(String badgesJpg) {
        this.badgesJpg = badgesJpg;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BadgesEntity that = (BadgesEntity) o;

        if (badgesId != that.badgesId) return false;
        if (badgesJpg != null ? !badgesJpg.equals(that.badgesJpg) : that.badgesJpg != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = badgesId;
        result = 31 * result + (badgesJpg != null ? badgesJpg.hashCode() : 0);
        return result;
    }
}
