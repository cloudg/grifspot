package models;

import javax.persistence.*;

/**
 * Created by tony on 12/6/14.
 */
@Entity
@Table(name = "Feedback_has_User", schema = "", catalog = "GrifSpot")
@IdClass(FeedbackHasUserEntityPK.class)
public class FeedbackHasUserEntity {
    private int feedbackFeedbackId;
    private int userUserId;

    @Id
    @Column(name = "Feedback_FeedbackID")
    public int getFeedbackFeedbackId() {
        return feedbackFeedbackId;
    }

    public void setFeedbackFeedbackId(int feedbackFeedbackId) {
        this.feedbackFeedbackId = feedbackFeedbackId;
    }

    @Id
    @Column(name = "User_UserID")
    public int getUserUserId() {
        return userUserId;
    }

    public void setUserUserId(int userUserId) {
        this.userUserId = userUserId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FeedbackHasUserEntity that = (FeedbackHasUserEntity) o;

        if (feedbackFeedbackId != that.feedbackFeedbackId) return false;
        if (userUserId != that.userUserId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = feedbackFeedbackId;
        result = 31 * result + userUserId;
        return result;
    }
}
