package models;

import javax.persistence.*;

/**
 * Created by tony on 12/6/14.
 */
@Entity
@Table(name = "Questions", schema = "", catalog = "GrifSpot")
public class QuestionsEntity {
    private int questionsId;
    private String question;

    @Id
    @Column(name = "QuestionsID")
    public int getQuestionsId() {
        return questionsId;
    }

    public void setQuestionsId(int questionsId) {
        this.questionsId = questionsId;
    }

    @Basic
    @Column(name = "Question")
    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QuestionsEntity that = (QuestionsEntity) o;

        if (questionsId != that.questionsId) return false;
        if (question != null ? !question.equals(that.question) : that.question != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = questionsId;
        result = 31 * result + (question != null ? question.hashCode() : 0);
        return result;
    }
}
