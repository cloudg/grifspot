package controllers;

import models.CategoryEntity;
import models.PoiEntity;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.*;

import views.html.*;

import java.util.ArrayList;
import java.util.List;

public class Application extends Controller {
    public static Result index() {
        return ok(index.render());
    }

    @Transactional
    public static Result allPoiAction(){
        String categoriesString = request().getQueryString("categories");
        String[] categories = null;

        ArrayList<Integer> categoryList = new ArrayList();
        if(categoriesString != null){
            categories = categoriesString.split(",");
            for(String category : categories){
                categoryList.add(Integer.parseInt(category));
            }
        }

        List<PoiEntity> list = null;
        if(categoryList.isEmpty()){
            list = PoiEntity.findAll();
        }else{
            list = PoiEntity.findInCategories(categoryList);
        }

        return ok(Json.toJson(list));
    }

    @Transactional
    public static Result poiAction(Integer i){
        PoiEntity poi = PoiEntity.findById(i);
        return ok(Json.toJson(poi));
    }

    @Transactional
    public static Result categoryAction(){
        List<CategoryEntity> categories = CategoryEntity.findAll();
        return ok(Json.toJson(categories));
    }

}
