
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object index extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/():play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.4*/("""

"""),_display_(/*3.2*/main("GrifSpot")/*3.18*/ {_display_(Seq[Any](format.raw/*3.20*/("""
    """),format.raw/*4.83*/("""
    """),format.raw/*5.5*/("""<div id="topOfPage" xmlns="http://www.w3.org/1999/html">
        <br>
        <center><img src="/assets/images/westminster_logo.jpg" size=50%></center>
        <div class="categories form">
            """),format.raw/*9.39*/("""
                """),format.raw/*10.17*/("""<center>
                    <strong><H2>GrifSpot</H2></strong>
               <strong><H3>Category Selection</H3></strong>
                <div ng-dropdown-multiselect=""  options="categories" selected-model="selectedModel"></div>
                <br>
                </center>
            """),format.raw/*16.23*/("""
        """),format.raw/*17.9*/("""</div>
    </div>

    <div class="col-lg-12">
        <div class="well clearfix" id="spotlight">
            <div class="col-md-6">
                <br>
                <br>
                <a href=""""),format.raw/*25.26*/("""{"""),format.raw/*25.27*/("""{"""),format.raw/*25.28*/("""spotlight.url"""),format.raw/*25.41*/("""}"""),format.raw/*25.42*/("""}"""),format.raw/*25.43*/(""""><img style="height: 300px;" src="/assets/images/"""),format.raw/*25.93*/("""{"""),format.raw/*25.94*/("""{"""),format.raw/*25.95*/("""spotlight.image"""),format.raw/*25.110*/("""}"""),format.raw/*25.111*/("""}"""),format.raw/*25.112*/(""""/></a>
            </div>
            <div class="col-md-6">
                <h1>"""),format.raw/*28.21*/("""{"""),format.raw/*28.22*/("""{"""),format.raw/*28.23*/("""spotlight.name"""),format.raw/*28.37*/("""}"""),format.raw/*28.38*/("""}"""),format.raw/*28.39*/("""</h1>
                <strong>"""),format.raw/*29.25*/("""{"""),format.raw/*29.26*/("""{"""),format.raw/*29.27*/("""spotlight.phoneNumber"""),format.raw/*29.48*/("""}"""),format.raw/*29.49*/("""}"""),format.raw/*29.50*/("""</strong><br>
                <strong>"""),format.raw/*30.25*/("""{"""),format.raw/*30.26*/("""{"""),format.raw/*30.27*/("""spotlight.address"""),format.raw/*30.44*/("""}"""),format.raw/*30.45*/("""}"""),format.raw/*30.46*/("""</strong><br><br>
                <div ng-if="spotlight.restaurantsByPointId.length > 0">
                    <br>
                    Type: <strong>"""),format.raw/*33.35*/("""{"""),format.raw/*33.36*/("""{"""),format.raw/*33.37*/("""spotlight.restaurantsByPointId[0].restType"""),format.raw/*33.79*/("""}"""),format.raw/*33.80*/("""}"""),format.raw/*33.81*/("""</strong><br>
                    Style: <strong>"""),format.raw/*34.36*/("""{"""),format.raw/*34.37*/("""{"""),format.raw/*34.38*/("""spotlight.restaurantsByPointId[0].style"""),format.raw/*34.77*/("""}"""),format.raw/*34.78*/("""}"""),format.raw/*34.79*/("""</strong><br>
                        Specialty: <strong>"""),format.raw/*35.44*/("""{"""),format.raw/*35.45*/("""{"""),format.raw/*35.46*/("""spotlight.restaurantsByPointId[0].specialty"""),format.raw/*35.89*/("""}"""),format.raw/*35.90*/("""}"""),format.raw/*35.91*/("""</strong><br>
                    Zagat Rating: <strong>"""),format.raw/*36.43*/("""{"""),format.raw/*36.44*/("""{"""),format.raw/*36.45*/("""spotlight.restaurantsByPointId[0].zagatRating"""),format.raw/*36.90*/("""}"""),format.raw/*36.91*/("""}"""),format.raw/*36.92*/("""</strong><br>
                        Price: <strong>"""),format.raw/*37.40*/("""{"""),format.raw/*37.41*/("""{"""),format.raw/*37.42*/("""spotlight.restaurantsByPointId[0].price"""),format.raw/*37.81*/("""}"""),format.raw/*37.82*/("""}"""),format.raw/*37.83*/("""</strong>
                </div>
                <div ng-if="spotlight.transportationsByPointId.length > 0">
                    <br>
                    Fares: <strong>"""),format.raw/*41.36*/("""{"""),format.raw/*41.37*/("""{"""),format.raw/*41.38*/("""spotlight.transportationsByPointId[0].fares"""),format.raw/*41.81*/("""}"""),format.raw/*41.82*/("""}"""),format.raw/*41.83*/("""</strong><br>
                    Type: <strong>"""),format.raw/*42.35*/("""{"""),format.raw/*42.36*/("""{"""),format.raw/*42.37*/("""spotlight.transportationsByPointId[0].ptType"""),format.raw/*42.81*/("""}"""),format.raw/*42.82*/("""}"""),format.raw/*42.83*/("""</strong><br>
                </div>
                <div ng-if="spotlight.architecturesByPointId.length > 0">
                    <br>
                    Type: <strong>"""),format.raw/*46.35*/("""{"""),format.raw/*46.36*/("""{"""),format.raw/*46.37*/("""spotlight.architecturesByPointId[0].archType"""),format.raw/*46.81*/("""}"""),format.raw/*46.82*/("""}"""),format.raw/*46.83*/("""</strong><br>
                    Purpose: <strong>"""),format.raw/*47.38*/("""{"""),format.raw/*47.39*/("""{"""),format.raw/*47.40*/("""spotlight.architecturesByPointId[0].purpose"""),format.raw/*47.83*/("""}"""),format.raw/*47.84*/("""}"""),format.raw/*47.85*/("""</strong><br>
                </div>

            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="col-lg-4"  ng-repeat="poi in pois" id=""""),format.raw/*54.60*/("""{"""),format.raw/*54.61*/("""{"""),format.raw/*54.62*/("""poi.name"""),format.raw/*54.70*/("""}"""),format.raw/*54.71*/("""}"""),format.raw/*54.72*/("""">
            <h3>"""),format.raw/*55.17*/("""{"""),format.raw/*55.18*/("""{"""),format.raw/*55.19*/("""poi.name"""),format.raw/*55.27*/("""}"""),format.raw/*55.28*/("""}"""),format.raw/*55.29*/("""</h3>
            <a href="#topOfPage" ng-click="updateSpotlight(poi)"><img style="width: 100%; height: 200px;" src="/assets/images/"""),format.raw/*56.127*/("""{"""),format.raw/*56.128*/("""{"""),format.raw/*56.129*/("""poi.image"""),format.raw/*56.138*/("""}"""),format.raw/*56.139*/("""}"""),format.raw/*56.140*/(""""/></a>
        </div>
    </div>
""")))}),format.raw/*59.2*/("""
"""))}
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sun Dec 07 16:29:59 MST 2014
                  SOURCE: /Users/charlesbailey/projects/gspot/app/views/index.scala.html
                  HASH: c579cddb133fa350cde6f291d6ef558264ce9a3a
                  MATRIX: 716->1|805->3|833->6|857->22|896->24|928->107|959->112|1188->340|1233->357|1552->658|1588->667|1816->867|1845->868|1874->869|1915->882|1944->883|1973->884|2051->934|2080->935|2109->936|2153->951|2183->952|2213->953|2323->1035|2352->1036|2381->1037|2423->1051|2452->1052|2481->1053|2539->1083|2568->1084|2597->1085|2646->1106|2675->1107|2704->1108|2770->1146|2799->1147|2828->1148|2873->1165|2902->1166|2931->1167|3108->1316|3137->1317|3166->1318|3236->1360|3265->1361|3294->1362|3371->1411|3400->1412|3429->1413|3496->1452|3525->1453|3554->1454|3639->1511|3668->1512|3697->1513|3768->1556|3797->1557|3826->1558|3910->1614|3939->1615|3968->1616|4041->1661|4070->1662|4099->1663|4180->1716|4209->1717|4238->1718|4305->1757|4334->1758|4363->1759|4560->1928|4589->1929|4618->1930|4689->1973|4718->1974|4747->1975|4823->2023|4852->2024|4881->2025|4953->2069|4982->2070|5011->2071|5209->2241|5238->2242|5267->2243|5339->2287|5368->2288|5397->2289|5476->2340|5505->2341|5534->2342|5605->2385|5634->2386|5663->2387|5861->2557|5890->2558|5919->2559|5955->2567|5984->2568|6013->2569|6060->2588|6089->2589|6118->2590|6154->2598|6183->2599|6212->2600|6373->2732|6403->2733|6433->2734|6471->2743|6501->2744|6531->2745|6596->2780
                  LINES: 26->1|29->1|31->3|31->3|31->3|32->4|33->5|37->9|38->10|44->16|45->17|53->25|53->25|53->25|53->25|53->25|53->25|53->25|53->25|53->25|53->25|53->25|53->25|56->28|56->28|56->28|56->28|56->28|56->28|57->29|57->29|57->29|57->29|57->29|57->29|58->30|58->30|58->30|58->30|58->30|58->30|61->33|61->33|61->33|61->33|61->33|61->33|62->34|62->34|62->34|62->34|62->34|62->34|63->35|63->35|63->35|63->35|63->35|63->35|64->36|64->36|64->36|64->36|64->36|64->36|65->37|65->37|65->37|65->37|65->37|65->37|69->41|69->41|69->41|69->41|69->41|69->41|70->42|70->42|70->42|70->42|70->42|70->42|74->46|74->46|74->46|74->46|74->46|74->46|75->47|75->47|75->47|75->47|75->47|75->47|82->54|82->54|82->54|82->54|82->54|82->54|83->55|83->55|83->55|83->55|83->55|83->55|84->56|84->56|84->56|84->56|84->56|84->56|87->59
                  -- GENERATED --
              */
          