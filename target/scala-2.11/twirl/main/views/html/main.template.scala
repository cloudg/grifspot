
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object main extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[String,Html,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(title: String)(content: Html):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.32*/("""

"""),format.raw/*3.1*/("""<!DOCTYPE html>

<html ng-app="gSpotApp">
    <head>
        <title>"""),_display_(/*7.17*/title),format.raw/*7.22*/("""</title>
        <link rel="stylesheet" media="screen" href=""""),_display_(/*8.54*/routes/*8.60*/.Assets.at("stylesheets/main.css")),format.raw/*8.94*/("""">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
        <link rel="shortcut icon" type="image/png" href=""""),_display_(/*10.59*/routes/*10.65*/.Assets.at("images/favicon.ico")),format.raw/*10.97*/("""">
        <script src=""""),_display_(/*11.23*/routes/*11.29*/.Assets.at("javascripts/angular/angular.js")),format.raw/*11.73*/("""" type="text/javascript"></script>
        <script src=""""),_display_(/*12.23*/routes/*12.29*/.Assets.at("javascripts/angular/angular-route.js")),format.raw/*12.79*/("""" type="text/javascript"></script>
        <script src=""""),_display_(/*13.23*/routes/*13.29*/.Assets.at("javascripts/angular/angular-sanitize.js")),format.raw/*13.82*/("""" type="text/javascript"></script>
        <script src=""""),_display_(/*14.23*/routes/*14.29*/.Assets.at("javascripts/angular/angular-touch.js")),format.raw/*14.79*/("""" type="text/javascript"></script>
        <script src=""""),_display_(/*15.23*/routes/*15.29*/.Assets.at("javascripts/angular/angular-animate.js")),format.raw/*15.81*/("""" type="text/javascript"></script>
        <script src=""""),_display_(/*16.23*/routes/*16.29*/.Assets.at("javascripts/angular/angular-resource.js")),format.raw/*16.82*/("""" type="text/javascript"></script>
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/lodash.js/2.4.1/lodash.min.js"></script>
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/highlight.js/8.0/highlight.min.js"></script>
        <script type="text/javascript" src="https://rawgit.com/pc035860/angular-highlightjs/master/angular-highlightjs.js"></script>
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.10.0/ui-bootstrap-tpls.js"></script>
        <script src=""""),_display_(/*21.23*/routes/*21.29*/.Assets.at("javascripts/angularjs-dropdown-multiselect-master/src/angularjs-dropdown-multiselect.js")),format.raw/*21.130*/("""" type="text/javascript"></script>
        <script src=""""),_display_(/*22.23*/routes/*22.29*/.Assets.at("javascripts/app/resource.js")),format.raw/*22.70*/("""" type="text/javascript"></script>
        <script src=""""),_display_(/*23.23*/routes/*23.29*/.Assets.at("javascripts/app/controller.js")),format.raw/*23.72*/("""" type="text/javascript"></script>
        <script src=""""),_display_(/*24.23*/routes/*24.29*/.Assets.at("javascripts/hello.js")),format.raw/*24.63*/("""" type="text/javascript"></script>
    </head>
    <body ng-controller="GSpotCtrl">
        <div class="container">
            """),_display_(/*28.14*/content),format.raw/*28.21*/("""
        """),format.raw/*29.9*/("""</div>
    </body>
    """),format.raw/*31.100*/("""
"""),format.raw/*32.1*/("""</html>
"""))}
  }

  def render(title:String,content:Html): play.twirl.api.HtmlFormat.Appendable = apply(title)(content)

  def f:((String) => (Html) => play.twirl.api.HtmlFormat.Appendable) = (title) => (content) => apply(title)(content)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sun Dec 07 16:11:18 MST 2014
                  SOURCE: /Users/charlesbailey/projects/gspot/app/views/main.scala.html
                  HASH: a7d4384b8e8d16224e65983a1720d25e094d06ba
                  MATRIX: 727->1|845->31|873->33|968->102|993->107|1081->169|1095->175|1149->209|1346->379|1361->385|1414->417|1466->442|1481->448|1546->492|1630->549|1645->555|1716->605|1800->662|1815->668|1889->721|1973->778|1988->784|2059->834|2143->891|2158->897|2231->949|2315->1006|2330->1012|2404->1065|3013->1647|3028->1653|3151->1754|3235->1811|3250->1817|3312->1858|3396->1915|3411->1921|3475->1964|3559->2021|3574->2027|3629->2061|3785->2190|3813->2197|3849->2206|3901->2324|3929->2325
                  LINES: 26->1|29->1|31->3|35->7|35->7|36->8|36->8|36->8|38->10|38->10|38->10|39->11|39->11|39->11|40->12|40->12|40->12|41->13|41->13|41->13|42->14|42->14|42->14|43->15|43->15|43->15|44->16|44->16|44->16|49->21|49->21|49->21|50->22|50->22|50->22|51->23|51->23|51->23|52->24|52->24|52->24|56->28|56->28|57->29|59->31|60->32
                  -- GENERATED --
              */
          