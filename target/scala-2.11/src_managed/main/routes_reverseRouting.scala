// @SOURCE:/Users/charlesbailey/projects/gspot/conf/routes
// @HASH:34fe71021967685fd2d78dcc7516f0a22b2033d6
// @DATE:Sun Dec 07 16:11:18 MST 2014

import Routes.{prefix => _prefix, defaultPrefix => _defaultPrefix}
import play.core._
import play.core.Router._
import play.core.Router.HandlerInvokerFactory._
import play.core.j._

import play.api.mvc._
import _root_.controllers.Assets.Asset
import _root_.play.libs.F

import Router.queryString


// @LINE:12
// @LINE:9
// @LINE:8
// @LINE:7
// @LINE:6
package controllers {

// @LINE:12
class ReverseAssets {


// @LINE:12
def at(file:String): Call = {
   implicit val _rrc = new ReverseRouteContext(Map(("path", "/public")))
   Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[PathBindable[String]].unbind("file", file))
}
                        

}
                          

// @LINE:9
// @LINE:8
// @LINE:7
// @LINE:6
class ReverseApplication {


// @LINE:7
def allPoiAction(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "points-of-interest")
}
                        

// @LINE:8
def poiAction(i:Integer): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "points-of-interest/" + implicitly[PathBindable[Integer]].unbind("i", i))
}
                        

// @LINE:9
def categoryAction(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "categories")
}
                        

// @LINE:6
def index(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix)
}
                        

}
                          
}
                  


// @LINE:12
// @LINE:9
// @LINE:8
// @LINE:7
// @LINE:6
package controllers.javascript {
import ReverseRouteContext.empty

// @LINE:12
class ReverseAssets {


// @LINE:12
def at : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Assets.at",
   """
      function(file) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("file", file)})
      }
   """
)
                        

}
              

// @LINE:9
// @LINE:8
// @LINE:7
// @LINE:6
class ReverseApplication {


// @LINE:7
def allPoiAction : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.allPoiAction",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "points-of-interest"})
      }
   """
)
                        

// @LINE:8
def poiAction : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.poiAction",
   """
      function(i) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "points-of-interest/" + (""" + implicitly[PathBindable[Integer]].javascriptUnbind + """)("i", i)})
      }
   """
)
                        

// @LINE:9
def categoryAction : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.categoryAction",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "categories"})
      }
   """
)
                        

// @LINE:6
def index : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.index",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + """"})
      }
   """
)
                        

}
              
}
        


// @LINE:12
// @LINE:9
// @LINE:8
// @LINE:7
// @LINE:6
package controllers.ref {


// @LINE:12
class ReverseAssets {


// @LINE:12
def at(path:String, file:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Assets.at(path, file), HandlerDef(this.getClass.getClassLoader, "", "controllers.Assets", "at", Seq(classOf[String], classOf[String]), "GET", """ Map static resources from the /public folder to the /assets URL path""", _prefix + """assets/$file<.+>""")
)
                      

}
                          

// @LINE:9
// @LINE:8
// @LINE:7
// @LINE:6
class ReverseApplication {


// @LINE:7
def allPoiAction(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.allPoiAction(), HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "allPoiAction", Seq(), "GET", """""", _prefix + """points-of-interest""")
)
                      

// @LINE:8
def poiAction(i:Integer): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.poiAction(i), HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "poiAction", Seq(classOf[Integer]), "GET", """""", _prefix + """points-of-interest/$i<[^/]+>""")
)
                      

// @LINE:9
def categoryAction(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.categoryAction(), HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "categoryAction", Seq(), "GET", """""", _prefix + """categories""")
)
                      

// @LINE:6
def index(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.index(), HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "index", Seq(), "GET", """ Home page""", _prefix + """""")
)
                      

}
                          
}
        
    