// @SOURCE:/Users/charlesbailey/projects/gspot/conf/routes
// @HASH:34fe71021967685fd2d78dcc7516f0a22b2033d6
// @DATE:Sun Dec 07 16:11:18 MST 2014


import play.core._
import play.core.Router._
import play.core.Router.HandlerInvokerFactory._
import play.core.j._

import play.api.mvc._
import _root_.controllers.Assets.Asset
import _root_.play.libs.F

import Router.queryString

object Routes extends Router.Routes {

import ReverseRouteContext.empty

private var _prefix = "/"

def setPrefix(prefix: String) {
  _prefix = prefix
  List[(String,Routes)]().foreach {
    case (p, router) => router.setPrefix(prefix + (if(prefix.endsWith("/")) "" else "/") + p)
  }
}

def prefix = _prefix

lazy val defaultPrefix = { if(Routes.prefix.endsWith("/")) "" else "/" }


// @LINE:6
private[this] lazy val controllers_Application_index0_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix))))
private[this] lazy val controllers_Application_index0_invoker = createInvoker(
controllers.Application.index(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "index", Nil,"GET", """ Home page""", Routes.prefix + """"""))
        

// @LINE:7
private[this] lazy val controllers_Application_allPoiAction1_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("points-of-interest"))))
private[this] lazy val controllers_Application_allPoiAction1_invoker = createInvoker(
controllers.Application.allPoiAction(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "allPoiAction", Nil,"GET", """""", Routes.prefix + """points-of-interest"""))
        

// @LINE:8
private[this] lazy val controllers_Application_poiAction2_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("points-of-interest/"),DynamicPart("i", """[^/]+""",true))))
private[this] lazy val controllers_Application_poiAction2_invoker = createInvoker(
controllers.Application.poiAction(fakeValue[Integer]),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "poiAction", Seq(classOf[Integer]),"GET", """""", Routes.prefix + """points-of-interest/$i<[^/]+>"""))
        

// @LINE:9
private[this] lazy val controllers_Application_categoryAction3_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("categories"))))
private[this] lazy val controllers_Application_categoryAction3_invoker = createInvoker(
controllers.Application.categoryAction(),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "categoryAction", Nil,"GET", """""", Routes.prefix + """categories"""))
        

// @LINE:12
private[this] lazy val controllers_Assets_at4_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("assets/"),DynamicPart("file", """.+""",false))))
private[this] lazy val controllers_Assets_at4_invoker = createInvoker(
controllers.Assets.at(fakeValue[String], fakeValue[String]),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Assets", "at", Seq(classOf[String], classOf[String]),"GET", """ Map static resources from the /public folder to the /assets URL path""", Routes.prefix + """assets/$file<.+>"""))
        
def documentation = List(("""GET""", prefix,"""controllers.Application.index()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """points-of-interest""","""controllers.Application.allPoiAction()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """points-of-interest/$i<[^/]+>""","""controllers.Application.poiAction(i:Integer)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """categories""","""controllers.Application.categoryAction()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """assets/$file<.+>""","""controllers.Assets.at(path:String = "/public", file:String)""")).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
  case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
  case l => s ++ l.asInstanceOf[List[(String,String,String)]]
}}
      

def routes:PartialFunction[RequestHeader,Handler] = {

// @LINE:6
case controllers_Application_index0_route(params) => {
   call { 
        controllers_Application_index0_invoker.call(controllers.Application.index())
   }
}
        

// @LINE:7
case controllers_Application_allPoiAction1_route(params) => {
   call { 
        controllers_Application_allPoiAction1_invoker.call(controllers.Application.allPoiAction())
   }
}
        

// @LINE:8
case controllers_Application_poiAction2_route(params) => {
   call(params.fromPath[Integer]("i", None)) { (i) =>
        controllers_Application_poiAction2_invoker.call(controllers.Application.poiAction(i))
   }
}
        

// @LINE:9
case controllers_Application_categoryAction3_route(params) => {
   call { 
        controllers_Application_categoryAction3_invoker.call(controllers.Application.categoryAction())
   }
}
        

// @LINE:12
case controllers_Assets_at4_route(params) => {
   call(Param[String]("path", Right("/public")), params.fromPath[String]("file", None)) { (path, file) =>
        controllers_Assets_at4_invoker.call(controllers.Assets.at(path, file))
   }
}
        
}

}
     