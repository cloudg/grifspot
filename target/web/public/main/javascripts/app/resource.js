
angular.module('poi', ["ngResource"]).factory('POI', function($resource) {
    return $resource('/points-of-interest/:id'); // Note the full endpoint address
});

angular.module('category', ["ngResource"]).factory('Category', function($resource) {
    return $resource('/categories'); // Note the full endpoint address
});