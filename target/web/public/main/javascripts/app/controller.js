var gspotApp = angular.module('gSpotApp', ['ngRoute', 'poi', 'category', 'angularjs-dropdown-multiselect']);

gspotApp.controller('GSpotCtrl', function ($scope, POI, Category, $timeout)
{
    $scope.pois = POI.query();
    Category.query({}, function(data){
        angular.forEach(data, function(value){
            value.label = value.name;
            console.log(value);
        });
        $scope.categories = data;
    });
    $scope.selectedModel = [{"id":1},{"id":2},{"id":3},{"id":4},{"id":5},{"id":6},{"id":7}] ;
    $scope.$watchCollection('selectedModel', function() {
        var idArray = [];
        angular.forEach($scope.selectedModel, function(value){
            idArray.push(value.id);
        });
        console.log(idArray);
        $scope.pois = POI.query({categories: idArray.toString()});
    });


    $timeout(function(){
        $scope.spotlight = $scope.pois[0];
    }, 100);

    $scope.changeCategories = function(){

    }

    $scope.updateSpotlight = function(spotlighted){
        $scope.spotlight = spotlighted;
    }
});